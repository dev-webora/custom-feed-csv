<?php

namespace Webora\CustomFeed\Controller\Adminhtml\Order;

use Magento\Backend\App\Action\Context;
use Magento\Sales\Model\Order;
use Magento\Directory\Api\CountryInformationAcquirerInterface;
use Magento\Framework\App\ResourceConnection;

class Export extends \Magento\Backend\App\Action
{
    private $order_model;
    private $countryInformationAcquirerInterface;
    private $separatore;
    private $enclosure;
    private $resource;
    
    public function __construct(
        Context $context,
        Order $order,
        CountryInformationAcquirerInterface $countryInformationAcquirerInterface,
        ResourceConnection $resource)
        {
        parent::__construct($context);
        $this->order_model = $order;
        $this->countryInformationAcquirerInterface = $countryInformationAcquirerInterface;
        $this->separatore = ';';
        $this->enclosure = '"';
        $this->resource = $resource;
        }    
    
    public function execute() {
        $headers = [
            "Numero Ordine",
            "Nome",
            "Cognome",
            "Indirizzo",
            "Nazione",
            "Regione",
            "Cap",
            "Città",
            "Marketplace",
            "Corriere",
            
            "Marca Mistral",
            "Sku",
            "Quantità",
            "Prezzo Unitario",
        ];
        #$headers = implode(";" , $headers) . "\n";
        
        $ids = $this->getRequest()->getParam('selected');
        $info = [];
        array_push($info, $headers);
        foreach($ids as $id){
            $order = $this->order_model->load($id);
            
            //For Marketplace
            $marketPlace = $this->getMarketPlaceCodePayment($id);
            if(empty($marketPlace)) {
                $title = $order->getPayment()->getAdditionalInformation('method_title');
                $marketPlace = $this->getWebsiteCodePayment($title);
            }
            
            //Order info
            $orderNumber = $order->getData('increment_id');
            $firstName = $order->getData('customer_firstname');
            $lastName = $order->getData('customer_lastname');
            

            //Address info
            $shippingAddress = $order->getShippingAddress();
            $street = $shippingAddress->getData('street');
            $region = $shippingAddress->getData('region');
            $postCode = $shippingAddress->getData('postcode');
            $city = $shippingAddress->getData('city');
            
            
            //For country
            $countryIdShipping = $shippingAddress->getCountryId();
            $countryNameShipping = $this->getCountryName($countryIdShipping);
            
            //For Courier service
                // Italia < 3kg= GLS
                // Italia > 3kg = TNT
                // Estero < 2 kg= GLS
                // Estero > 2kg TNT
            $weight = intval($order->getData('weight'));    
            if($countryIdShipping == 'IT'){
                $corriere = $weight < 3 ? 'GLS' : 'TNT';
            }else{
                $corriere = $weight < 2 ? 'GLS' : 'TNT';
            }
            
            
            //Push all info in array
            $intestazione = array($orderNumber, $firstName, $lastName, $street, $countryNameShipping, $region, $postCode, $city, $marketPlace, $corriere);
            
           
            
            foreach($order->getAllItems() as $item){
                //get details from single item
                
                // if(!empty($item->getProduct())){
                //     $mistral = $item->getProduct()->getData('marca_mistral');
                // }else $mistral = '';
                
                $mistral = empty($item->getProduct()) ? '' : $item->getProduct()->getData('marca_mistral');
                
                $sku = $item->getData('sku');
                $quantity = intval($item->getData('qty_ordered'));
                $price = number_format($item->getData('base_row_total_incl_tax'), 2);
                $details = array($mistral, $sku, $quantity, $price);
                
                //push in details
                array_push($info, array_merge($intestazione,$details));
            }
               
            
           
            // echo $headers;
        }
        
        $output = '';
        foreach($info as $val){
            $output.= $this->enclosure.implode($this->enclosure.$this->separatore.$this->enclosure , $val) . $this->enclosure."\n";
        }
        
        $data = date('dmY');
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=feed_$data.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
            
        echo $output;
    }
    
    /**
     * Getting Country Name
     * @param string $countryCode
     * @param string $type
     *
     * @return null|string
     * */
    private function getCountryName($countryCode, $type="local"){
        $countryName = null;
        try {
            $data = $this->countryInformationAcquirerInterface->getCountryInfo($countryCode);
            if($type == "local"){
                $countryName = $data->getFullNameLocale();
            }else {
                $countryName = $data->getFullNameLocale();
            }
        } catch (NoSuchEntityException $e) {}
        return $countryName;
    }
    
    private function getMarketPlaceCodePayment($orderId){
            // Marketplace di acquisto (codice per Ebay: 130 – Codice per Amazon 140 – Codice per Amazon Prime: 145 – Codice Per PayPal: 150 -  Codice per PostePay: 160 - Codice per contrassegno: 170
            $channel=$this->getChannel($orderId);
            
            switch($channel){
                case 'ebay':
                    return '130';
                    break;
                case 'amazon':
                    if($this->isPrime($orderId)){
                        return '145';
                        break;
                    }else{
                        return '140';
                        break;
                    }
                default: return '';
            }
    }
    
    private function getChannel($orderId){
        $connection  = $this->resource->getConnection();
        $tableName = $connection->getTableName("m2epro_order");
        
        $select = $connection->select()->from($tableName, 'component_mode')->where('magento_order_id = :order_id');
        
        $bind = [':order_id' => (string)$orderId];
        
        $result = (string)$connection->fetchOne($select, $bind);
         
        #$sql= 'SELECT component_mode FROM m2epro_order WHERE magento_order_id = '.$orderId; 
        if(empty($result)) return 'sito';
        return $result;
    }
    
    private function isPrime($orderId){
        $connection  = $this->resource->getConnection();
        $m2epro_order = $connection->getTableName("m2epro_order");
        $m2epro_amazon_order = $connection->getTableName("m2epro_amazon_order");
        $select = $connection->select()->from($m2epro_order, $m2epro_amazon_order.'.is_prime')->join($m2epro_amazon_order, $m2epro_order.".id = ".$m2epro_amazon_order.".order_id")->where('magento_order_id = :order_id');
        $bind = [':order_id' => (string)$orderId];
        $result = (string)$connection->fetchOne($select, $bind);
        
        #$sql = 'SELECT m2epro_amazon_order.is_prime FROM m2epro_order a JOIN m2epro_amazon_order b on a.id = b.order_id WHERE magento_order_id = $orderId';
        return $result;
    }
    
    private function getWebsiteCodePayment($title){
        switch($title){
            case 'paypal': return '150'; break;
            case 'postepay': return '160'; break;
            case 'contrassegno': return '170'; break;
        }
    }
    
}